// For outputting data or text into the browser console
console.log('Hello World')

// [SYNTAX AND STATEMENTS]
// syntax is the code that makes up a statement
// console
// log ()

// Statements are made up of syntax that will be run by the browser
// example: console.log()

// [COMMENTS]
// This is a single-line comment for short descriptions


/*
This is a multi-line comment for longer description and multi0;ine paragraphs.
*/

// [VARIABLES]
// let variables are variables that CAN be re-assigned
let firstName = 'Earl';
console.log(firstName)

let lastName = 'Musk';
console.log(lastName)

// Re-assigning a value to a let variable shsows no errors
firstName = 'Elon';
console.log(firstName)


// const variables are variables that CANNOT be re-assigned
const colorOfTheSun = 'yellow';
console.log('The color of the sun is ' + colorOfTheSun)

// Error happens when you try to re-assign value of a const variable
// colorOfTheSun = 'red'
// console.log(colorOfTheSun)

// When declaring a variable, you use a keyword like 'let'
let variableName = 'Value'

// When re-assigning a value to a variable, you just need the variable name
variableName = 'New Value'


// DATA TYPES
// 1. String - Denoted by single OR double quotation marks
let personName = 'Earl Diaz';

// 2. Number - no quotation marks and numerical value
let personAge = 15

// 3. Boolean - Only 'true' or 'false'
let hasGirlfriend = false;

// 4. Array - Denoted by brakets and can contain multiple values inside
let hobbies = ['Cycling', 'Reading', 'Coding']

// Object - Denoted by curly braces and has value name/label for each value.
let person = {
	personName: 'Earl Diaz',
	personAge: 15,
	hasGirlfriend: false,
	hobbies: ['Cycling', 'Reading', 'Coding']
}

// 6. Null - is a placeholder for future variable re-assignments
let wallet = null;


// console.log each of the variables
console.log(personName)
console.log(personAge)
console.log(hasGirlfriend)
console.log(hobbies)
console.log(person)
console.log(wallet)

// // to display single value of an object
console.log(person.personAge)

// to display single
console.log(hobbies[1])
